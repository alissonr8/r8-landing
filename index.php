<?php

    $imgPath = "./assets/imgs/";

    $text_partnership = [
        'title' => "Nossa parceira, <span>É seus benefícios</span>",
        'text' => "Sites incríveis a preços inacreditáveis! Olá, se você chegou até aqui é porque está interessado em construir seu próprio e-commerce, não é mesmo? A Tray é uma excelente plataforma, pensada para ajudar empresas de todos os tamanhos a construir e gerenciar um comércio digital. E nós? Nós ajudamos você com design e programação, para que tudo funcione da forma que deve."
    ];

    $text_certificate = [
        'title' => " Recursos TRAY",
        'text' => "<p> Sites incríveis a preços inacreditáveis! </p> <p> Olá, se você chegou até aqui é porque está interessado em construir seu próprio e-commerce, não é mesmo? A Tray é uma excelente plataforma, pensada para ajudar empresas de todos os tamanhos a construir e gerenciar um comércio digital. E nós? Nós ajudamos você com design e programação, para que tudo funcione da forma que deve. </p>"
    ];

    $text_advantages = "Recursos TRAY <span> O melhor que a TRAY oferece: </span>";

    $content_advantages = [
        [
            'title' => "Marketplace",
            'text' => "Venda nos maiores marketplaces do Brasil de forma mais simples e rápida. Tenha total controle de suas operações.",
            'icon' => $imgPath."marketplace-icon.svg"
        ],
        [
            'title' => "Integrações",
            'text' => "A plataforma conta com integrações de sistemas para meios de pagamento, gerenciadores de risco, comparadores de preços, marketplaces e muito mais.",
            'icon' => $imgPath."integration-icon.svg"
        ],
        [
            'title' => "Personalização Completa",
            'text' => "O seu site do seu jeito, 100% personalizável. Mude o tema, acrescente detalhes, tenha um site que além de ser a sua cara, foi feito exclusivamente para você.",
            'icon' => $imgPath."personalize-icon.svg"
        ]
    ];

    $text_services = [
        'title' => "Serviços <span> Prestados a você! </span>",
        'text' => "Veja a seguir como funciona o processo para criação de sua loja personalizada"
    ];

    $content_services = [
        [
            'title' => "Personalização da Loja",
            'text' => "Enviamos para você um layout exclusivo, criado por nós, para aprovação. Você faz as modificações que achar necessárias e juntos criamos a cara do seu site. ",
            'icon' => $imgPath."personalize-service.svg"
        ],
        [
            'title' => "Implementação",
            'text' => "Depois de aprovado o layout, toda a programação do site na plataforma da Tray fica por nossa conta. Nessa fase, você relaxa enquanto fazemos o trabalho pesado.",
            'icon' => $imgPath."implementation-service.svg"
        ],
        [
            'title' => "Treinamento",
            'text' => "Assim que terminamos a implementação da sua loja na Tray, oferecemos um treinamento completo para que você se torne um expert e opere seu site com facilidade.",
            'icon' => $imgPath."training-service.svg"
        ],
        [
            'title' => "Marketing Digital",
            'text' => "Nossos serviços não param por aqui. Depois de colocar seu site no ar, caso você queira, ajudamos você com marketing digital e campanhas que farão sua loja bombar!",
            'icon' => $imgPath."marketing-service.svg"
        ],
    ];

    $text_presentation = [
        'title' => "Nossos <span>Serviços</span>",
        'text' => "Criamos e-commerces personalizados para você. Juntos, fazemos todos os processos necessários para a criação de uma loja online que vende. Além disso, trabalhamos também com marketing digital, para dar aquele empurrãozinho que seu negócio precisa."
    ];

    $text_contact = [
        'title' => "Serviços",
        'text' => "Por favor preencha o formulário abaixo e um de nossos consultores irá entrar em contato com você em breve."
    ];

    $content_options = [
        [
            'value' => "Personalizacao",
            'text' => "Personalização da Loja"
        ],
        [
            'value' => "Implementacao",
            'text' => "Implementação de Loja"
        ],
        [
            'value' => "Treinamento",
            'text' => "Treinamento"
        ],
        [
            'value' => "Marketing",
            'text' => "Marketing Digital"
        ],
    ];

    $text_flying = [
        'title' => "Quer decolar <span>sozinho?</span>",
        'text' => "Prefere fazer tudo por sua conta? Sem problemas. Clicando no botão DECOLAR você ganha 20% de desconto na Tray para montar seu e-commerce sozinho. Boas vendas!"
    ];

?>

<!DOCTYPE html>
<html>

<head>

    <title>Agência R8 e TRAY Parceria </title>

    <link rel="author" href="https://www.agenciar8.com.br" />
    <link rel="icon" href="./assets/imgs/favicon.png" />

    <meta http-equiv="Content-Type" content="text/xhtml; charset=UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <link rel="stylesheet" href="./assets/node_modules/bootstrap/dist/css/bootstrap.min.css" />
    <link rel="stylesheet" href="./assets/style.css" />

    <script src="./assets/node_modules/jquery/dist/jquery.min.js"></script>
    <script defer src="https://use.fontawesome.com/releases/v5.1.1/js/all.js" integrity="sha384-BtvRZcyfv4r0x/phJt9Y9HhnN5ur1Z+kZbKVgzVBAlQZX4jvAuImlIz+bG7TS00a" crossorigin="anonymous"></script>
    <script src="./assets/node_modules/bootstrap/dist/js/bootstrap.min.js"></script>
    <script src="./assets/node_modules/jquery-mask-plugin/dist/jquery.mask.min.js"></script>
    <script src="./assets/javascript.js"></script>

</head>

<body>

    <!-- Modal -->
    <div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">

                <div class="modal-header">
                    <h5 class="modal-title text-center" id="exampleModalCenterTitle">Parabéns</h5>
                    <!-- <button type="button" class="close" data-dismiss="modal" aria-label="Close"> -->
                    <!-- <span aria-hidden="true">&times;</span> -->
                    </button>
                </div>

                <div class="modal-body text-center">
                    <div class=""> Você ganhou um cupom de 20% de desconto no primeiro mês! </div>
                    <div class=""> Aproveite: </div>
                    <div class="cupom"> PARCEIROS20 </div>
                </div>

                <div class="modal-footer flex-h-centered">
                    <a type="button" style="-webkit-appearance: initial" target="_blank" class="ct-bt bk-yellow" href="http://www.tray.com.br/parceria/agenciar8/"> Quero decolar </a>
                    <button type="button" class="ct-bt bk-black" data-dismiss="modal"> Vou dar mais uma olhada! </button>
                </div>

            </div>
        </div>
    </div>

    <header style="position: relative;">

        <section class="d-none d-lg-block floating-bts">
            <div class="bts-containter full-width">
                <div class="fixer">
                    <button class="ct-bt bk-yellow margin-t-15px" data-toggle="modal" data-target="#exampleModalCenter" >
                        Cupom Desconto TRAY
                    </button>
                    <a class="ct-bt bk-black float-right margin-t-15px" href="#cttoForm">
                        Fale Com Consultor
                    </a>
                </div>
            </div>
        </section>

        <div class="padding-tb-30px bk-gradient-yellow-t">
            <div class="container">
                <div class="col-12">

                    <div class="row justify-content-between">

                        <div class="logos-container flex-h-centered float-left col-12 col-lg-4">
                            
                            <a href="https://www.agenciar8.com.br" target="_blank">
                                <img src="./assets/imgs/r8-logo.png" alt="Agência R8" />
                            </a>

                            <div class="right-separator"></div>

                            <img src="./assets/imgs/tray-logo.png" alt="Tray E-commerce" />

                        </div>

                    </div>

                    <div class="row margin-t-75px">

                        <div class="col-12 col-lg-6">

                            <div class="flag-yellow text-uppercase">
                                <h2 class="font-250"> <?php echo $text_partnership['title'] ?> </h2>
                            </div>

                            <div class="row certificate margin-t-15px">
                                <div class="col-12 col-md-3">
                                    <img src="./assets/imgs/certificado-tray.png" alt="Certificado TRAY">
                                </div>
                                <div class="custom-text col-12 col-md-9">
                                    <?php echo $text_partnership['text'] ?>
                                </div>
                            </div>

                        </div>

                        <div class="col-12 col-lg-6">
                            <img class="img-fluid" src="./assets/imgs/site-build.png" alt="Construção de Sites">
                        </div>

                    </div>

                </div>
            </div>
        </div>

        <section class="sect-advantages padding-tb-30px">
            <div class="container">

                <div class="col-12 col-lg-9 margin-t-15px">

                    <div class="text-center text-uppercase">
                        <h2 class="font-250"> <?php echo $text_advantages ?> </h2>
                    </div>

                    <div class="content row margin-t-45px">

                    <?php foreach($content_advantages as $key => $item): ?>

                        <div class="col-12 col-lg-4">
                            <div class="card text-center full-width">
                                <img class="card-img-top ct-icon" src="<?php echo $item['icon'] ?>" alt="<?php echo $item['title'] ?>" />
                                <div class="card-body">
                                    <h5 class="card-title font-bold text-uppercase"> <?php echo $item['title'] ?> </h5>
                                    <p class="card-text text-justify">
                                        <?php echo $item['text'] ?>
                                    </p>
                                </div>
                            </div>
                        </div>

                    <?php endforeach; ?>

                    </div>

                </div>

            </div>
        </section>

        <section class="sect-pre-service padding-t-30px">

            <div class="container full-height">
                <div class="col-12 col-lg-9 full-height">
                    <div class=" pre-content row full-height align-items-center">

                        <div class="col-10 col-lg-7 full-height">

                            <div class="flag-black text-uppercase">
                                <h2 class="font-250"> <?php echo $text_services['title'] ?> </h2>
                            </div>

                            <div class="content text-justify margin-t-45px">
                                <?php echo $text_services['text'] ?>
                            </div>
                        </div>

                    </div>
                </div>
            </div>

            <div class="triangle-effect"></div>

        </section>

        <section class="sect-service padding-b-75px">

            <div class="container padding-b-75px">

                <div class="col-12 col-lg-9">

                    <div class="row time-line">

                        <div class="left-line col-6 col-md-5">

                            <img class="ct-icon element" src="./assets/imgs/personalize-service.svg" />

                            <div class="line-text element">
                                <h4 class="text-uppercase font-bold"> Implementação </h4>
                                <p>
                                    Depois de aprovado o layout, toda a programação do site na plataforma da Tray fica por nossa conta. Nessa fase, você relaxa enquanto fazemos o trabalho pesado.
                                </p>
                            </div>

                            <img class="ct-icon element" src="./assets/imgs/training-service.svg" />

                            <div class="line-text element no-bottom">
                                <h4 class="text-uppercase font-bold"> Marketing Digital </h4>
                                <p>
                                    Nossos serviços não param por aqui. Depois de colocar seu site no ar, caso você queira, ajudamos você com marketing digital e campanhas que farão sua loja bombar!
                                </p>
                            </div>
                        </div>


                        <div class="divisory flex-v-centered d-none d-md-flex col-md-1">

                            <div class="circle"></div>
                            <div class="line"></div>
                            <div class="circle"></div>
                            <div class="line"></div>
                            <div class="circle"></div>
                            <div class="line"></div>
                            <div class="circle"></div>

                        </div>

                        <div class="right-line col-6 col-md-5">

                            <div class="line-text element">
                                <h4 class="text-uppercase font-bold"> Personalização da loja </h4>
                                <p>
                                    Enviamos para você um layout exclusivo, criado por nós, para aprovação. Você faz as modificações que achar necessárias e juntos criamos a cara do seu site. 
                                </p>
                            </div>

                            <img class="ct-icon element" src="./assets/imgs/implementation-service.svg" />

                            <div class="line-text element">
                                <h4 class="text-uppercase font-bold"> Treinamento </h4>
                                <p>
                                    Assim que terminamos a implementação da sua loja na Tray, oferecemos um treinamento completo para que você se torne um expert e opere seu site com facilidade.
                                </p>
                            </div>

                            <img class="ct-icon element no-bottom" src="./assets/imgs/marketing-service.svg" />

                        </div>

                    </div>

                </div>

            </div>

        </section>

    </header>

    <section class="sect-presentation bk-gradient-yellow-b padding-tb-30px">

        <div class="container full-height">

            <div class="col-12 full-height">
                <div class=" pre-content row full-height align-items-center">

                    <div class=" col-12 col-lg-3 full-height">

                        <div class="flag-black text-uppercase">
                            <h2 class="font-250"> <?php echo $text_presentation['title'] ?> </h2>
                        </div>

                        <div class="content text-justify margin-t-45px">
                            <?php echo $text_presentation['text'] ?>
                        </div>
                    </div>

                    <div class="col-12 col-lg-9">
                        <img class="img-fluid margin-t-n-105px" src="./assets/imgs/devices-mockup.png" />
                    </div>

                </div>
            </div>

        </div>

    </section>

    <section style="position: relative;" class="sect-ctto padding-tb-30px">
        <div class="container">
            <div class="row">

                <div class="col-12 col-lg-4">
                    <div class="flag-yellow text-uppercase full-width">
                        <h2 class="font-250"> <?php echo $text_contact['title'] ?> </h2>
                        <p> <?php echo $text_contact['text'] ?> </p>
                    </div>
                </div>

            </div>

            <div class="row">

                <div class="col-12 col-lg-8">

                    <form class="ctto-form" data-destiny="https://agenciar8.com.br/form" id="cttoForm">

                        <input type="hidden" name="token" value="8IlJGHh2sLjxpRxvtslbAePk2" />
                        <input type="hidden" name="destinatario_form" value="alisson@agenciar8.com.br" />
                        <input type="hidden" name="replyTo" value="alisson@agenciar8.com.br" />
                        <input type="hidden" name="empresa" value="Contato pela Landingpage com a TRAY" />

                        <div class="row">
                            <div class="col-6">
                                <input required placeholder="Nome*" name="Personal['Name']" />
                            </div>
                            <div class="col-6">
                                <input required placeholder="E-mail*" type="mail" class="mail-mask" onChange="checkMailPattern(this)" name="Personal['Email']" />
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-6">
                                <input required placeholder="Telefone*" class="phone-mask" name="Personal['Phone']" />
                            </div>

                            <div class="col-6">

                                <span class="downCaret">
                                    <i class="fas fa-angle-down"></i>
                                </span>

                                <div class="fake-input c-pointer" onClick="showOption()"> O que você precisa?* </div>

                                <input hidden placeholder="O que você precisa?*" name="Help['I need help with']" id="ajudaInput" />

                                <div class="select-ground" focusout="hideOption()">

                                    <?php $i = 0; foreach($content_options as $key => $item): ?>
                                        <div class="custom-control custom-checkbox">
                                            <input type="checkbox" onChange="setOption('<?php echo $item['value'] ?>', this)" class="custom-control-input" id="customCheck<?php echo $i ?>">
                                            <label class="custom-control-label" for="customCheck<?php echo $i ?>"> <?php echo $item['text'] ?> </label>
                                        </div>
                                    <?php $i++; endforeach; ?>
                                    
                                </div>

                            </div>
                        </div>

                        <div class="row">
                            <div class="col-12">
                                <textarea placeholder="Mensagem*" required name="Message['I want']"></textarea>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-12">
                                <button class="ct-bt bk-yellow" type="submit">
                                    Enviar
                                </button>
                            </div>
                        </div>

                    </form>

                </div>

            </div>

        </div>
    </section>

    <footer>

        <section class="sect-footer bk-yellow">

            <div class="container">

                <div class="row">

                    <div class="footer-content col-12 col-lg-4 padding-tb-30px">

                        <div class="flag-black text-uppercase">
                            <h2 class="font-250"> <?php echo $text_flying['title'] ?> </h2>
                        </div>

                        <p class="margin-t-15px">
                            <?php echo $text_flying['text'] ?>
                        </p>

                        <a class="ct-bt bk-white" target="_blank" href="http://www.tray.com.br/parceria/agenciar8/">
                            Decolar
                        </a>

                    </div>

                    <div class="col-12 col-lg-8">
                        <img src="./assets/imgs/decolar.png" alt="Decolando" />
                    </div>

                </div>

            </div>

        </section>

        <section class="signature padding-tb-30px  bk-black">
            <div class="container text-center text-white">
                <p class="no-padding no-margin" style="margin-top: -7px;">
                    © 2013-2018 Agência R8. Todos os direitos reservados. · Política de Privacidade.
                </p>
                <p class="no-padding no-margin">
                    Feito com <i class="fas fa-heart"></i> no Udi City por nós mesmos.
                </p>
            </div>
        </section>

    </footer>

    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-45171184-1"></script>
    <script>
    window.dataLayer = window.dataLayer || [];
    function gtag(){dataLayer.push(arguments);}
    gtag('js', new Date());

    gtag('config', 'UA-45171184-1');
    </script>

    <!-- Facebook Pixel Code -->
    <script>
    !function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
    n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
    n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
    t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
    document,'script','https://connect.facebook.net/en_US/fbevents.js');

    fbq('init', '320038175006851');
    fbq('track', "PageView");</script>
    <noscript><img height="1" width="1" style="display:none"
    src="https://www.facebook.com/tr?id=320038175006851&ev=PageView&noscript=1"
    /></noscript>
    <!-- End Facebook Pixel Code -->

    <!-- BEGIN JIVOSITE CODE {literal} -->
		<script type='text/javascript'>
		(function(){ var widget_id = 'adv8XULGxO';var d=document;var w=window;function l(){
		var s = document.createElement('script'); s.type = 'text/javascript'; s.async = true; s.src = '//code.jivosite.com/script/widget/'+widget_id; var ss = document.getElementsByTagName('script')[0]; ss.parentNode.insertBefore(s, ss);}if(d.readyState=='complete'){l();}else{if(w.attachEvent){w.attachEvent('onload',l);}else{w.addEventListener('load',l,false);}}})();</script>
		<!-- {/literal} END JIVOSITE CODE -->
        
		<script>
		  (function (w,i,d,g,e,t,s) {w[d] = w[d]||[];t= i.createElement(g);
		    t.async=1;t.src=e;s=i.getElementsByTagName(g)[0];s.parentNode.insertBefore(t, s);
		  })(window, document, '_gscq','script','//widgets.getsitecontrol.com/62331/script.js');
		</script>

</body>

</html>