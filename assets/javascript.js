$(document).ready(
    function() {

        $("#cttoForm").on("submit", function(e){

            e.preventDefault();
            var form = $(this).serialize();


            if( $(".mail-mask").attr("data-invalid") != true ) {

                if($(".phone-mask").val().length > 12) {

                    $.ajax({
                        data: form,
                        method: "POST",
                        dataType: "json",
                        url: $(this).attr("data-destiny"),
                        success: function(){
                            alert("Recebemos sua mensagem com sucesso!");
                        },
                        error: function(jqXHR, textStatus, errorThrown) {
                            alert("Ouve falha ao enviar a mensagem!");
                            console.log(jqXHR);
                            console.log(textStatus);
                            console.log(errorThrown);
                        }
                    });

                } else {
                    alert("Atenção! \n digite um número de telefone completo.");
                }

            } else {
                alert("Atenção! \n O campo de e-mail está inválido, por favor digite um e-mail válido");
            }

        })

        fixHeight($(".sect-service .time-line .element"));

        $('.phone-mask').mask('(99) 99999-9999');

    }
)

function fixHeight(elem) {
    var max = 0;

    elem.each(function() {
        if ($(this).height() > max) {
            max = $(this).height();
        }
    })

    elem.css("height", max + "px");

}

function checkMailPattern(field) {
    var pattern = new RegExp("^[a-z0-9.]+@[a-z0-9]+\.[a-z]+\.([a-z]+)?$", "gi");
    if( !pattern.test(field.value) ){
        field.setAttribute("data-invalid", "true");
    } else {
        field.setAttribute("data-invalid", "false");
    }
}

function showOption() {
    $(".select-ground").addClass("active");
    $(".select-ground").on("mouseleave", function(){
        $(".select-ground").removeClass("active");
    })
    // $(".clicableBlock").css("display", "block");
}

function hideOption() {
    // $(".select-ground").removeClass("active");
    // $(".clicableBlock").css("display", "none");
}

function setOption(option, check) {

    $elem = $("#ajudaInput");
    $fakeElem = $(".ctto-form .fake-input");
    var rg;

    if(check.checked){
        
        if( $elem.val() == "" || $elem.val() == undefined ){
            $elem.val(option);
            $fakeElem.html( $elem.val() );
        }  else {
            $elem.val($elem.val()+"; "+option);
            $fakeElem.html( $elem.val() );
        }        

    } else {

        rg = new RegExp("; "+option, "gi");
        
        if( rg.test($elem.val()) ) {
            $elem.val( $elem.val().replace("; "+option, "") );
        } else {
            $elem.val( $elem.val().replace(option,"") );
        }

        if( $elem.val() == "" || $elem.val() == undefined ){
            $fakeElem.html( "O que você precisa?*" );
        } else {
            $fakeElem.html( $elem.val() );
        }
    }
}